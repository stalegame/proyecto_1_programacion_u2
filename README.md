# Proyecto_1_programación_U2

Basicamente mi proyecto, esta pensando como un gestor de stock de una bodega, donde los puntos claves son, el formato de los distintos cilindros, la cantidad y su estado(lleno o vacio).
Utilizo una interfaz bastante simple, donde en la ventana principal ocupo 3 botones, un expander y un treeview.
En primera instancia tengo botones basicos que serian; eliminar, editar y añadir. Donde el más importante seria el "añadir", por que este abre el expander en caso de que no este desplegado, donde pasa a ver 2 cuadros de texto y una combobox (con los estados posibles), aqui al pinchar este boton, se añade al treeview, con forma de columnas y filas.
El boton editar, permite tomar la informacion que decida el usario y cambiarla a gusto, aqui se plegara el expander y mostrara la informacion, con el añadido de dos botones auxiliares, donde puedes cancelar la edicion, o confirmar los cambios.
Y por ultimo el boton eliminar, sirve para dicho proposito, de eliminar filas del treeview y por ende eliminarlos del json.

A decir verdad, por falta de tiempo no pude implementar una mejor gestion para restar o sumar cantidad de llenos y vacios, de una forma diferente, pero no quede para nada desconforme con el resultado final, siento que cumple con su funcion, puede que todavia tenga algunos errores puntuales, pero con las pruebas que realice, senti que funcionaba bastante bien.

Y por ultimo, pero no menos importante utilice dos ventanas de dialogo, unas para los errores, por ejemplo al no ingresar los datos a los cuadros de texto, se toma como error para que no queden espacios en blanco, o de confirmacion, al momento de eliminar datos.
