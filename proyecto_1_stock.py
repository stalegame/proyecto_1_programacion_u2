"""Proyecto 1 - U2 Thomas Aranguiz Gaete"""
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import json


# Se ocupa la funcion para obtener el elemto del combobox
def cargar_combo(combo):

    combo_aux = combo.get_active_iter()

    # si el combobox tiene algo seleccionado, se obtiene de la lista el valor
    if combo_aux is not None:
        model = combo.get_model()
        name = model[combo_aux][0]
    else:

        name = None

    return name


# Se crea la funcion, para comprobar los datos
def comprobar(new_formato, new_cantidad, new_estado, datos, old_name):

    # Se usan estas dos como  variable, como pivot para comprobar que
    # tipo de error usar
    verificar = True
    error = None

    # Comprueba que no se repita el formato y estado a la vez
    try:
        for x in range(len(datos[new_estado])):
            if datos[new_estado][x][0] == new_formato:
                error = 'repetido'
                verificar = False
    except KeyError:
        pass

    # Si no existe nada manda el error, para que al casilla no quede en blanco
    if new_formato == '':
        error = 'formato'
        verificar = False

    elif new_cantidad == '':
        error = 'cantidad'
        verificar = False

    elif new_estado is None:
        error = 'estado'
        verificar = False

    return verificar, error


# Funcion para obtener datos del archivo json
def cargar_json():

    try:
        # Se carga el archivo json y se almacena en datos
        with open('stock_register.json') as file:
            datos = json.load(file)

    # En caso de no existir el archivo json, se crea la variable datos
    except IOError:
        datos = {}

    return datos


# Funcion para guardar datos en archivo json
def guardar_datos(datos):

    with open('stock_register.json', 'w') as file:
        json.dump(datos, file, indent=4)


# Se crea la clase, para programar los widget que estan en el glade,
# de esta ventana
class window():

    def __init__(self):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('plantilla.glade')

        # Se programa la ventana
        self.ventana = self.builder.get_object('main_window')
        self.ventana.set_default_size(400, 800)
        self.ventana.connect('destroy', Gtk.main_quit)

        # Se reconocen los botonos, con sus funciones
        self.add_btn = self.builder.get_object('add_btn')
        self.add_btn.connect("clicked", self.add_btn_clicked)

        self.edit_btn = self.builder.get_object('edit_btn')
        self.edit_btn.connect("clicked", self.edit_btn_clicked)

        self.del_btn = self.builder.get_object('del_btn')
        self.del_btn.connect("clicked", self.del_btn_clicked)

        # Se carga el expander
        self.expander = self.builder.get_object('expander')

        # Se carga la liststore y el treeview
        self.lista = Gtk.ListStore(str, str, str)
        self.tree = self.builder.get_object('main_tree')
        self.tree.set_model(model=self.lista)

        celda = Gtk.CellRendererText()

        titulos = ('formato', 'cantidad', 'estado')

        for i in range(3):
            columnas = Gtk.TreeViewColumn(titulos[i], celda, text=i)
            self.tree.append_column(columnas)

        # Se cargan los entry y la combobox
        self.formato_entry = self.builder.get_object('formato')
        self.cantidad_entry = self.builder.get_object('cantidad')
        self.estado_combo = self.builder.get_object('estado')

        self.mostar_datos()
        self.ventana.show_all()


    # Metodo para añadir formatos
    def add_btn_clicked(self, btn=None):

        # se cargan los entry y los datos del combobox
        formato = self.formato_entry.get_text()
        cantidad = self.cantidad_entry.get_text()
        estado = cargar_combo(self.estado_combo)

        # Se cargan los datos del json
        datos = cargar_json()

        # Se ocupa la dupla, para retornar el error (en caso de haber error)
        valido, aux = comprobar(formato, cantidad, estado, datos, None)

        if valido:

            if estado not in datos:
                datos[estado] = []

            # Se forma un arreglo con dos daos y se añade a datos
            add = [formato, cantidad]

            datos[estado].append(add)

            # se guardan los datos en el json, limpia la pantalla y carga
            # los nuevos datos
            guardar_datos(datos)
            self.formato_entry.set_text('')
            self.cantidad_entry.set_text('')
            self.estado_combo.set_active(-1)

            self.limpiar()
            self.mostar_datos()
            self.ventana.show_all()

        else:


            if not self.expander.get_expanded():
                self.expander.set_expanded(True)

            error_dialog(aux)


    # Metodo para limpiar datos en pantalla
    def limpiar(self):

        if len(self.lista) != 0:

            # Se remueve selectivamente los datos (uno por uno) todos
            # los elementos de la lista
            for i in range(len(self.lista)):
                iterar = self.lista.get_iter(0)
                self.lista.remove(iterar)


    # Metodo para cargar y mostrarr datos almacenados
    def mostar_datos(self):

        datos = cargar_json()

        # se leen cada elemento del json y se crea una tupla 3 elementos
        # este contiene formato, cantidad, estado
        for tipo in datos:
            for elementos in datos[tipo]:
                elementos.append(tipo)
                print(elementos)
                self.lista.append(elementos)

    # Metodo para eliminar toda la fila y eliminar del archivo
    def del_btn_clicked(self, btn=None):

        # Se obtiene el elemento de la lista(list) y el iterador(la fila)
        # actual de la zona seleccion por el usario
        list, iter = self.tree.get_selection().get_selected()

        # Validacion no seleccionado
        if list is None or iter is None:
            return

        datos = cargar_json()

        confirmar = delete_dialog(list.get_value(iter, 0))
        response = confirmar.del_window.run()

        # Saca el nombre y estado, en dos variable y al momento de
        # encontrarlos en el archivo, los elimina
        estado_original = list.get_value(iter, 2)

        if response == Gtk.ResponseType.OK:

            for x in range(len(datos[estado_original])):
                if datos[estado_original][x][0] == list.get_value(iter, 0):
                    datos[estado_original].remove(datos[estado_original][x])

        guardar_datos(datos)
        self.limpiar()
        self.mostar_datos()


    # Metodo para editar datos
    def edit_btn_clicked(self, btn=None):

        # Se obtiene el elemento de la lista(list) y el iterador(la fila)
        # actual de la zona seleccion por el usario
        lis, iter = self.tree.get_selection().get_selected()

        if list is None or iter is None:
            return

        # con los valores ya establecidos, se ponen nuevamente en los entry,
        # para ser editados
        self.formato_entry.set_text(self.lista.get_value(iter, 0))
        self.cantidad_entry.set_text(self.lista.get_value(iter, 1))

        self.old_name = self.formato_entry.get_text()

        # Se utiliza para comprobar el estado y que no ocurran errores
        if self.lista.get_value(iter, 2) == 'Lleno':
            self.estado_combo.set_active(0)

        else:
            self.estado_combo.set_active(1)


        # Se agregan dos botones (guardar y cancelar) ubicados
        # debajo de los entry
        self.confirm = Gtk.Button(label='Guardar')
        self.cancel = Gtk.Button(label='Cancelar')

        # Se obtiene el grip en el que estan contenidos los entry con sus
        # label, para poder agregarle elementos
        self.aux_grid = self.builder.get_object('add_grid')
        self.aux_grid.attach(self.confirm, 1, 3, 1, 1)
        self.aux_grid.attach(self.cancel, 0, 3, 1, 1)

        # En caso de que el expander no este desplegado, se expande
        if not self.expander.get_expanded():
            self.expander.set_expanded(True)
        self.ventana.show_all()

        # Se conectan los botones
        self.confirm.connect('clicked', self.guardar_cambios)
        self.cancel.connect('clicked', self.cancelar_edit)

    # Metodo para cargar los cambios en caso de apretar el boton guardar
    def guardar_cambios(self, btn=None):

        # Se obtienen los datos de los entry y el combo
        new_formato = self.formato_entry.get_text()
        new_cantidad = self.cantidad_entry.get_text()
        new_estado = cargar_combo(self.estado_combo)

        datos = cargar_json()

        # Se ocupa la dupla, para retornar el error (en caso de haber error)
        valid, aux = comprobar(new_formato, new_cantidad, new_estado,
                               datos, self.old_name)

        # En caso de ser validos los datos, se cambian y se guardan,
        # para volver a ser mostrados
        if valid:

            for tipo in datos:
                for elementos in datos[tipo]:
                    if elementos[0] == self.old_name:
                        datos[tipo].remove(elementos)

            # Se añade todo a la lista
            add = [new_formato, new_cantidad]

            datos[new_estado].append(add)

            guardar_datos(datos)

            # Se limpia a los valores
            self.formato_entry.set_text('')
            self.cantidad_entry.set_text('')
            self.estado_combo.set_active(-1)

            self.limpiar()
            self.mostar_datos()

            # Se eliminan los botones de guardar y cancelar debido
            # a que es propio de editar
            self.confirm.destroy()
            self.cancel.destroy()

        else:

            if not self.expander.get_expanded():
                self.expader.set_expanded(True)
            error_dialog(aux)


    def cancelar_edit(self, btn=None):

        # Se destruyen ambos botones (guardar y cancelar)
        # y se limpia los valores
        self.cancel.destroy()
        self.confirm.destroy()
        self.formato_entry.set_text('')
        self.cantidad_entry.set_text('')
        self.estado_combo.set_active(-1)


# Ventana de error en caso de no ingresar datos validos
class error_dialog():

    # Constructor de la ventana y recibe el tipo de error.
    def __init__(self, error):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('plantilla.glade')
        self.error = error

        self.error_window = self.builder.get_object('error_window')
        self.error_window.show_all()

        self.error_btn = self.builder.get_object('error_ok')
        self.error_btn.connect('clicked', self.cerrar_dialog)

        # En caso de error de repetir estado, se muestra al usario
        if self.error == 'repetido':
            self.error_window.set_markup('El nombre ingresado ya se encuentra'
                                         ' registrado')

        # En caso de error de falta de completar casilla, se muestra al usario
        else:
            self.error_window.set_markup('Por favor, complete las casillas')

    # Metodo que cierra la ventana de error
    def cerrar_dialog(self, btn=None):
        self.error_window.destroy()

# Ventana que confirma querer borrar algun elemento
class delete_dialog():

    # Constructor de la ventana ademas recibe los
    def __init__(self, delete):

        self.builder = Gtk.Builder()
        self.builder.add_from_file('plantilla.glade')

        self.del_window = self.builder.get_object('del_window')
        self.del_window.show_all()

        # Se crean dos botones,uno para confirmar la delecion del elemento
        # y otro para cancelar
        self.del_ok = self.builder.get_object('del_ok')
        self.del_ok.connect('clicked', self.cerrar_dialog)
        self.del_cancel = self.builder.get_object('del_cancel')
        self.del_cancel.connect('clicked', self.cerrar_dialog)

        # Mensaje para asegurar que si desea eliminar la fila
        self.del_window.set_markup('¿Seguro que desea este elemento?')

    def cerrar_dialog(self, btn=None):
        self.del_window.destroy()


if __name__ == "__main__":
    window()
    Gtk.main()
